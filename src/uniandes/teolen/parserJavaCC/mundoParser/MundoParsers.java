package uniandes.teolen.parserJavaCC.mundoParser;

import java.util.ArrayList;
import java.util.HashMap;

import uniandes.teolen.parserJavaCC.newParser.ParserProyecto1;
import uniandes.teolen.parserJavaCC.propList.Simple;



public class MundoParsers {

	// Nombres de los Parsers
	private  ArrayList  <String> parsers  = new ArrayList <String> (); 

	// Parser que se est� usando
	private int currentParser;


	public  MundoParsers () 
	{

		// Agreguen al final de esta lista los nombres del nuevo parser

		parsers.add("ParserProyecto1");
		parsers.add("Simple");

		currentParser =  0;

	}

	public ParserProyecto1 getNuevoParser()
	{
		return new ParserProyecto1(System.in);
	}
	public Simple getParserSimple()
	{
		return new Simple(System.in);
	}

	public String getStringCurrentParser(){
		return parsers.get(currentParser);
	}

	public int getCurrentParser() {
		return currentParser;
	}

	public void setCurrentParser(int p) {
		currentParser = p;
	}

	public String getParser(int i) {
		return parsers.get(i);
	}

	public int sizeParsers() {
		return parsers.size();
	}

	public String  procesarCadena(String texto) {
		String resp = "";

		if(parsers.get(currentParser).equals("ParserProyecto1"))
		{
			ParserProyecto1 nuevoParser = getNuevoParser();
			nuevoParser.ReInit(new java.io.StringReader(texto));
			try {
				int[] n=nuevoParser.one_line();    	 //Metodo principal
				resp = new String("OK   "+ "\n"+"Hay "+ n[0]+" referencias en total"+"\n"+"Hay "+ n[1]+" articulos"+"\n"+"Hay "+ n[2]+" libros"+"\n"+"Hay "+ n[3]+" booklet"+"\n"+"Hay "+ n[4]+" manual"+"\n"+"Hay "+ n[5]+" masterthesis"+"\n"+"Hay "+ n[6]+" conferences"+"\n"+"Hay "+ n[7]+" inproceedings"+"\n"+"Hay "+ n[8]+" inbooks"+"\n"+"Hay "+ n[9]+" incollections"+"\n"+"Hay "+ n[10]+" miscs"+"\n"+"Hay "+ n[11]+" phdthesis"+"\n"+"Hay "+ n[12]+" proceedings"+"\n"+"Hay "+ n[13]+" techreports"+"\n"+"Hay "+ n[14]+" unpublisheds" );

			}
			catch (Exception e) 
			{
				resp = new String ("Error de Sintaxis: "+e.getMessage());
			}
			catch (Error e) 
			{
				resp = new String ("Error Lexico: "+e.getMessage());
			}
		}

		if(parsers.get(currentParser).equals("Simple"))
		{
			Simple nuevoParser = getParserSimple();
			nuevoParser.ReInit(new java.io.StringReader(texto));
			try {
				HashMap<String, Object> x=nuevoParser.simple();    	 //Metodo principal

				resp = new String("OK   "+ "\n"+"Hay "+" referencias");

			}
			catch (Exception e) 
			{
				resp = new String ("Error de Sintaxis: "+e.getMessage());
			}
			catch (Error e) 
			{
				resp = new String ("Error Lexico: "+e.getMessage());
			}
		}

		return "\n SISTEMA " + parsers.get(currentParser) + ": " + resp + "\n";
	}

}
